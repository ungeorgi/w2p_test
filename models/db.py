db = DAL('postgres://kmnmzmrhfqjyah:23944071793a3d81a157e648047fe3bb4a720c10ad1f5af8d5831edee3fe523a@ec2-23-20-205-19.compute-1.amazonaws.com:5432/de2ug7bci762n')

from gluon.contrib.appconfig import AppConfig
from gluon.tools import Auth
from gluon.tools import Crud
auth = Auth(db)
auth.define_tables()
crud = Crud(db)

db.define_table('empresa',
    Field('razon_social'),
    Field('fantasia'),
    Field('iut'),
    Field('giro'),
    Field('creada_en', 'datetime', default=request.now),
    Field('creada_por', 'reference auth_user', default=auth.user_id),
    format='%(fantasia)s')

db.define_table('ciudad',
    Field('nombre_ciudad'),
    format='%(nombre_ciudad)s')

db.define_table('comuna',
    Field('nombre_comuna'),
    Field('ciudad_id', 'reference ciudad'),
    format='%(nombre_comuna)s')

db.define_table('direccion',
    Field('empresa_id', 'reference empresa'),
    Field('direccion'),
    Field('comuna_id', 'reference comuna'),
    Field('creado_en', 'datetime', default=request.now),
    Field('creado_por', 'reference auth_user', default=auth.user_id))

db.empresa.iut.requires = IS_NOT_IN_DB(db, 'empresa.iut')
db.empresa.iut.requires = IS_NOT_EMPTY()
db.empresa.razon_social.requires = IS_NOT_EMPTY()
db.empresa.creada_por.readable = db.empresa.creada_por.writable = False
db.empresa.creada_en.readable = db.empresa.creada_en.writable = False

db.direccion.direccion.requires = IS_NOT_EMPTY()
db.direccion.creado_por.readable = db.direccion.creado_por.writable = False
db.direccion.creado_en.readable = db.direccion.creado_en.writable = False
db.direccion.empresa_id.readable = db.direccion.empresa_id.writable = False